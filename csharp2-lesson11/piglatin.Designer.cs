﻿namespace csharp2_lesson11
{
    partial class piglatin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.answerlabel = new System.Windows.Forms.Label();
            this.wordtextbox = new System.Windows.Forms.TextBox();
            this.translatebutton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.firstnamebox = new System.Windows.Forms.TextBox();
            this.lastnamebox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 138);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Word:";
            // 
            // answerlabel
            // 
            this.answerlabel.AutoSize = true;
            this.answerlabel.Location = new System.Drawing.Point(25, 240);
            this.answerlabel.Name = "answerlabel";
            this.answerlabel.Size = new System.Drawing.Size(42, 13);
            this.answerlabel.TabIndex = 1;
            this.answerlabel.Text = "Answer";
            // 
            // wordtextbox
            // 
            this.wordtextbox.Location = new System.Drawing.Point(105, 138);
            this.wordtextbox.Name = "wordtextbox";
            this.wordtextbox.Size = new System.Drawing.Size(126, 20);
            this.wordtextbox.TabIndex = 2;
            // 
            // translatebutton
            // 
            this.translatebutton.Location = new System.Drawing.Point(105, 184);
            this.translatebutton.Name = "translatebutton";
            this.translatebutton.Size = new System.Drawing.Size(75, 23);
            this.translatebutton.TabIndex = 3;
            this.translatebutton.Text = "Translate";
            this.translatebutton.UseVisualStyleBackColor = true;
            this.translatebutton.Click += new System.EventHandler(this.translatebutton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "First Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Last Name";
            // 
            // firstnamebox
            // 
            this.firstnamebox.Location = new System.Drawing.Point(105, 25);
            this.firstnamebox.Name = "firstnamebox";
            this.firstnamebox.Size = new System.Drawing.Size(126, 20);
            this.firstnamebox.TabIndex = 6;
            // 
            // lastnamebox
            // 
            this.lastnamebox.Location = new System.Drawing.Point(105, 88);
            this.lastnamebox.Name = "lastnamebox";
            this.lastnamebox.Size = new System.Drawing.Size(126, 20);
            this.lastnamebox.TabIndex = 7;
            // 
            // piglatin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.lastnamebox);
            this.Controls.Add(this.firstnamebox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.translatebutton);
            this.Controls.Add(this.wordtextbox);
            this.Controls.Add(this.answerlabel);
            this.Controls.Add(this.label1);
            this.Name = "piglatin";
            this.Text = "Pig Latin";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label answerlabel;
        private System.Windows.Forms.TextBox wordtextbox;
        private System.Windows.Forms.Button translatebutton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox firstnamebox;
        private System.Windows.Forms.TextBox lastnamebox;
    }
}

