﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp2_lesson11
{
    public partial class piglatin : Form
    {
        public piglatin()
        {
            InitializeComponent();
        }

        private void translatebutton_Click(object sender, EventArgs e)
        {
            string word = wordtextbox.Text;
            answerlabel.Text = makePigLatin(ref word,firstnamebox.Text,lastnamebox.Text);
        }

        public string makePigLatin(ref string word, string firstName, string lastName)
        {
            string newfn = "";
            string newln = "";
            string newword = "";
            if (firstName[0] == Convert.ToChar("a") || firstName[0] == Convert.ToChar("i") || firstName[0] == Convert.ToChar("e") || firstName[0] == Convert.ToChar("o") || firstName[0] == Convert.ToChar("u"))
            {
                newfn = firstName + "way";
            }
            else if (firstName[0] == Convert.ToChar("y"))
            {
                newfn = firstName + "yay";
            }
            else
            {
                newfn = firstName + "ay";
            }

            if (lastName[0] == Convert.ToChar("a") || lastName[0] == Convert.ToChar("i") || lastName[0] == Convert.ToChar("e") || lastName[0] == Convert.ToChar("o") || lastName[0] == Convert.ToChar("u"))
            {
                newln = lastName + "way";
            }
            else if (word[0] == Convert.ToChar("y"))
            {
                newln = lastName + "yay";
            }
            else
            {
                newln = lastName + "ay";
            }
            if (word[0] == Convert.ToChar("a") || word[0] == Convert.ToChar("i") || word[0] == Convert.ToChar("e") || word[0] == Convert.ToChar("o") || word[0] == Convert.ToChar("u"))
            {
                newword = word + "way";
            }
            else if (word[0] == Convert.ToChar("y"))
            {
                newword = word + "yay";
            }
            else{
                newword = word + "ay";
            }

            return newfn+" "+newln+" "+newword;
        }




    }
}
